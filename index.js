// console.log('hello');
	
	// 1
	let getCube = (num) => {
		cube = num ** 3; 
		console.log(`The cube of ${num} is ${cube}`);
	}
	getCube(2);


	// 2
	let address = ['258', 'Washington Ave NW', 'California', '90011'];

	let [houseNumber, street, state, zipCode] = address;
	   
	console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);


	// 3
	let animal= {
		name: "Lolong",
		specie:"Crocodile" ,
		weight: 1075,
		measurement: [20, 3] 
	};

	let {name, specie, weight, measurement} = animal;
	// console.log(name);
	// console.log(specie);
	// console.log(weight);
	// console.log(measurement);

	console.log(`${name} was a saltwater ${specie}. He wieghed at ${weight} kgs with a measurement of ${measurement[0]} ft ${measurement[1]} in.`);

	// 4
	let numbers = [1,2,3,4,5,15];

	numbers.forEach((number) => {
		console.log(number);
	});


	// 5
	class Dog {
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}

	let myDog = new Dog();
	myDog.name = "Frankie";
	myDog.age = 5;
	myDog.breed = "Miniature Dachshund"; 

	console.log(myDog);


